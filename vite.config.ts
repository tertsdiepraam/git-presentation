import { defineConfig } from 'vite';

export default defineConfig(({ command, mode }) => {
  if (command === "build"){
    return {
      base: "/git-presentation/"
    };
  } else {
    return {};
  }
})
