import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import "reveal.js/dist/reveal.css";
import "reveal.js/dist/theme/white.css";
import Highlight from "reveal.js/plugin/highlight/highlight.esm.js";
import "./atom-one-light.css";
import "./style.css";

let deck = new Reveal({
   plugins: [ Markdown, Highlight ]
})

deck.initialize();

