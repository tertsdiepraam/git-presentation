# The&nbsp;Big&nbsp;Review&nbsp;Review v2.0: Electric Boogaloo (Git Edition)

Prototype Monthly, May 10 2022

---

Let's talk about
![Git logo](images/Git-logo.svg)

-V-

**Git** (/ɡɪt/) _noun_, _derogatory_, _informal_

An unpleasant or contemptible person

_"that mean old Git"_

-V-

_From Wikipedia:_

Torvalds sarcastically quipped about the name Git:

> "I'm an egotistical bastard, and I name all my projects after myself."

_"Git"_ can mean anything, depending on your mood.

- Random three-letter combination that is pronounceable.
- _"Global information tracker"_: you're in a good mood, and it actually works for you.
- _"Goddamn idiotic truckload of sh*t"_: when it breaks.

-V-

## Why learn (more) Git?

- Saves time
- Lower chance of accidentally deleting data
- Prepared for projects that care about Git history

-V-
## If you care about Git history:

- Easily revert single changes
- Find the context for a change
- Find developer responsible for a change
- In short: amplifies Git's capabilities

-V-

In this presentation:

- Tips & Tricks
- Rewriting history
- Merge conflicts

All based on examples

-V-

Not in this presentation:

- Git's internal representation
- GUI's

-V-

Before we continue:

### Where to learn about Git?

```bash
git help [command]
# or
git [command] --help
```

or the online [docs](https://git-scm.com/docs) or the free [book](https://git-scm.com/book/en/v2)

I actually recommend reading the book

(just the parts you're interested in)

-V-

**Use whatever interface you're comfortable/productive with**

<span class="fragment">but</span>

<span class="fragment">**CLI is the common baseline**</span>

-V-

You should know basic Git CLI commands to:

- Follow tutorials/stackoverflow answers
- Work on remote servers
- Work on other computers
- To switch between GUI's and editors

-V-

I like to mix CLI and Git operations in my editor

-V-

### Git commands

![git commands](images/git-commands.png)

-V-

In this presentation:
- `init`
- `add`
- `status`
- `diff`
- `show`
- `commit`
- `reset`
- `branch`
- `checkout`
- `merge`
- `log`
- `push`
- `cherry-pick`
- `rebase`
- `revert`

---

## Time for some live coding!

![hackerman from Kung Fury](images/hackerman.gif)

<div class="fragment">

**Warning**: This involves bash scripts I wrote last
night between 11PM and 2AM.... Things _will_ go wrong.

(I'm terrified, why am I doing this to myself?)

</div>

---

You can find scripts to create the examples yourself here:

https://gitlab.com/tertsdiepraam/git-presentation/-/tree/main/examples

---

scenario 1
# Fight the Formatter

-V-

### Steps

- Set up a remote
- Commit some change with a formatting problem
- Push to remote

Uh, oh the formatter in the CI is complaining...

-V-

### Solution

```bash
git commit --amend
```

and to override on the remote:

```bash
git push -f
```

This will delete the previous commit!

(or at least make it very hard to recover)

**Note**: Some branches are _protected_ and don't allow force pushing

-V-

### Steps

- Set up a remote
- Commit some change with a formatting problem
- **Commit some good change**
- Push to remote

Uh, oh the formatter in the CI is complaining...

</li>

</ul>

<hr>
<div class="fragment">

### Solution

```bash
git rebase -i 
```

and `edit` the first commit, then:

```bash
git push -f
```

(This will delete the previous commit!)

</div>

-V-

But what if there's loads of commits with formatting problems!

<hr>
<div class="fragment">

### Solution

```bash
git rebase --exec "black ."
```

and amend the commits in question

</div>

-V-

### Summary

- `git commit --amend` for changing the last commit
- `git rebase -i` for more complex edits
- `git push -f` to overwrite the remote

---

scenario 2
# Hack the History

-V-

### Steps

- Make a file `A` and commit
- Make a file `B` and commit
- Make a file `C` and commit
- Make a file called `SECRETS` and commit
- Add something to file `A` and commit

-V-

### Goal

Now **revert** one of the commits

<hr>

<div class="fragment">

### Solution

```bash
git revert [commit]
```

This is easy, but does not delete from history

</div>

-V-

### Goal

- `B` should have been created before `A`
- All changes to `A` should go into 1 commit
- `SECRETS` should be deleted from the history
- The commit to `C` and the revert commit should be removed

-V-

### Solution

```bash
git rebase -i HEAD~3
```

and change this file to:

<div style="display: flex; gap: 0.5em;"> 

```txt
pick ac2f538 Added A.txt
pick 0f4128b Added B.txt
pick acc3d67 Added C.txt
pick baeb392 SECRETS
pick 9c77a3b Changed A.txt
pick 5bf4c42 Revert "Added C.txt"
```

```txt
pick 0f4128b Added B.txt
pick ac2f538 Added A.txt
squash 9c77a3b Changed A.txt
```

</div>

You might want to set:
```bash
git config core.editor [your editor of choice]
```

-V-

**Note**: If you've pushed to a remote, then removing from history is much more complicated.
GitHub has a [good solution](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository)
on their FAQ

-V-

### Summary
- `git revert` for _adding_ commits that undo other commits
- `git rebase -i` for complex changes

---
scenario 3
# Bandy the Branches

-V-

### Steps

```bash
git checkout first-branch
git commit -m "..."
```

### Goal

That commit should have been on `new-branch`

<hr>

<div class="fragment">

### Solution

```bash
git checkout -b new-branch
git checkout first-branch
git reset HEAD~
```

If `N` commits should be moved:

```bash
git reset HEAD~N
```

</div>

-V-

### About reset

- `--soft` only reset HEAD and current branch
- `--mixed` (default) also change index
- `--hard` also change working tree

Watch out with `--hard`!

-V-

### Specifying commits (and commit ranges)

```bash
git show 1c002dd4b536e7479fe34593e72e6c6c1819e53b # full SHA1
git show 1c002d # short SHA1
git show fix-all-bugs # branch name
git show HEAD # current commit
git show HEAD~ # parent commit
git show HEAD~4 # 4 commits ago
git show main~4 # main, but 4 commits back
git show main..HEAD # everything in HEAD but not main
git show main..fix-all-bugs # everything in fix-all-bugs but not main
git show HEAD~4..HEAD # last 4 commits
```

`git show` will simply show metadata and a diff

-V-

### Steps

```bash
git checkout A
git checkout -b B
git commit -m "..."
git checkout A
git checkout -b C
git commit -m "..."
git commit -m "..."
```

### Goal

The last commit on `C` should have been on `B`

-V-

### Solution

```bash
git checkout B
git cherry-pick C
git checkout C
git reset HEAD~
```

If, for instance, the last 2 commits should be copied:
```bash
git cherry-pick C~2..C
```

Or all commits on `C` that are not on `B`:
```bash
git cherry-pick B..C
```
-V-

### Summary

- `git reset HEAD~N` to move branch back `N` commits
- `git cherry-pick [range]` to copy commits

---

scenario 4
# Crush the Conflicts

-V-

By far the most requested feature in the survey
-V-

### Steps

```bash
# start on branch A
git checkout -b B
git commit -m "..." # edit a line
git checkout A
git commit -m "..." # edit the same line
git merge B
```

Uh oh, merge conflicts!

-V-

### Solution

![panic!!!!](images/panic.gif)

-V-

### ~Solution~

Honestly though

- There is no silver bullet
- You have to understand both changes
- You have to decide what to with it

<div class="fragment">

But there are some strategies

</div>

-V-

### Strategy 0

- Keep commits small
- Keep your branch up to date with `main`/`develop`

-V-

### Strategy 1

Many changes but history is clean?

<div class="fragment">

`rebase`!

</span>

<div class="fragment">

- Conflicts appear per commit
- Each conflict is small (hopefully)
- Can be tedious

</div>

-V-

### Strategy 2

- Try to fix it
- Test
- Reset a file to the conflict if it failed:

```bash
git checkout --conflict [file]
```

-V-

### Strategy 3

Check out the base and the two changes, not just the changes

Before:
```txt
def factorial(n):
<<<<<<< HEAD
    return 1 if not n else n * factorial(n - 1)
=======
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)
>>>>>>> branch2

print(f"{factorial(5) = }")
```

-V-

### Strategy 3

Check out the base and the two changes, not just the changes

After `git checkout --conflict=diff3 factorial.py`
```txt
def factorial(n):
<<<<<<< ours
    return 1 if not n else n * factorial(n - 1)
||||||| base
    raise NotImplementedError()
=======
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)
>>>>>>> theirs

print(f"{factorial(5) = }")
```

(You can set this as default in new git versions)

---

# This is the end

(there's a summary below)

-V-

Tips and tricks summary

- `git commit --amend`
- `git reset --hard` for "rewinding" a branch
- `git rebase -i` for complex operations
- `git rebase` for piecewise merging
- `git checkout --conflict[=diff3]` for resetting conflict and better conflict markers
- `git cherry-pick` for copying commits to another branch
- Watch out with `git push -f`
- [ohshitgit.com](https://ohshitgit.com)
- [Removing sensitive data](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository)

-V-

Third-party tools

- [Difftastic](https://github.com/Wilfred/difftastic): Syntax-aware diff
- [Delta](https://github.com/dandavison/delta): Another cool diff tool
- [Gitlens](https://gitlens.amod.io/): VS Code extension for more Git functionality
- [Starship](https://starship.rs/): Shell prompt with Git info
