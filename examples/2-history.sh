rm -rf 2-history
mkdir 2-history
cd 2-history

git init
git commit -m "Initial commit" --allow-empty
echo -e "This is file A.txt\n" > A.txt
git add A.txt
git commit -m "Added A.txt"

echo -e "This is file B.txt\n" > B.txt
git add B.txt
git commit -m "Added B.txt"

echo -e "This is file C.txt\n" > C.txt
git add C.txt
git commit -m "Added C.txt"

echo -e "SECRET-ACCESS-CODE\n" > SECRET.txt
git add SECRET.txt
git commit -m "Oops! Added a secret"

echo -e "This is another line in A.txt\n" >> A.txt
git add A.txt
git commit -m "Changed A.txt"

