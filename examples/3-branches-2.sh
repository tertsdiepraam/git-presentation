rm -rf 3-branches
mkdir 3-branches
cd 3-branches

git init
git commit -m "Initial commit" --allow-empty

git checkout -b branch-A
echo "Change on A" >> file.txt
git add file.txt
git commit -m "Change on A"

git checkout -b branch-B
echo "Change on B" >> file.txt
git add file.txt
git commit -m "Change on B"

git checkout branch-A

git checkout -b branch-C
echo "Change on C" >> file.txt
git add file.txt
git commit -m "Change on C"

echo "This is another file" >> another-file.txt
git add another-file.txt
git commit -m "Added another-file.txt"

