rm -rf 3-branches
mkdir 3-branches
cd 3-branches

git init
git commit -m "Initial commit" --allow-empty

echo "This change should have been on new-branch" > file.txt
git add file.txt
git commit -m "Added file.txt"

