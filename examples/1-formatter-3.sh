sh 1-formatter.sh

cd 1-formatter

echo "def gauss_sum(  n   ):
    res = 0
    for i in range( n    ):
        res += i
    return res

print(f\"{gauss_sum(5) = }\")
" >> functions.py

git add functions.py
git commit -m "Added sum function"
git push

