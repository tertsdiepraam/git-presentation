rm -rf 1-formatter
rm -rf 1-formatter-remote

mkdir 1-formatter
mkdir 1-formatter-remote

cd 1-formatter-remote
git init
git config receive.denyCurrentBranch ignore
cd ..

cd 1-formatter
git init
git remote add origin ../1-formatter-remote
git commit -m "Initial commit" --allow-empty
git push -u origin main

echo "def factorial(n):
    if n==0:
        return 1
    else:
        return n*factorial(n-1)

print(f\"{factorial(5) = }\")
" > functions.py

git add functions.py
git commit -m "Added factorial function"
git push

