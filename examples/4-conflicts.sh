rm -rf 4-conflicts
mkdir 4-conflicts
cd 4-conflicts

git init
git commit -m "Initial commit" --allow-empty

echo "def factorial(n):
    raise NotImplentedError()

print(f\"{factorial(5) = }\")
" > factorial.py
git add factorial.py
git commit -m "Added factorial.py"

echo "def gauss_sum(n):
    raise NotImplementedError()

print(f\"{gauss_sum(100) = }\")
" > gauss_sum.py

git add gauss_sum.py
git commit -m "Added gauss_sum.py"

git checkout -b left
echo "def factorial(n):
    return n * factorial(n - 1) if n else 1

print(f\"{factorial(5) = }\")" > factorial.py

git add factorial.py
git commit -m "Left change on factorial.py"

echo "def gauss_sum(n):
    \"\"\"
    Calculates the sum of the numbers 0 up to n
    \"\"\"
    raise NotImplementedError()

print(f\"{gauss_sum(100) = }\")
" > gauss_sum.py

git add gauss_sum.py
git commit -m "Left change to gauss_sum.py"

git checkout main
git checkout -b right
echo "from functools import reduce
from operator import mul

def factorial(n):
    return reduce(mul, range(1, n+1))

print(f\"{factorial(5) = }\")" > factorial.py
git add factorial.py
git commit -m "Right change on factorial.py"

echo "def gauss_sum(n):
    return sum(range(n+1))

print(f\"{gauss_sum(100) = }\")
" > gauss_sum.py

git add gauss_sum.py
git commit -m "Right change to gauss_sum.py"
